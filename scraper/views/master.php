<!DOCTYPE HTML>
<html ng-app="phpscrape">
	<head>
		<title>Scrape Post</title>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	</head>
	<body ng-controller="ScrapeController as scraper" ng-cloak>
		<div class="container">
			<div class="starter-template">
				<h1>PHP Scraper</h1>
				<button ng-disabled="scraper.fetching" ng-click="scraper.getFiveHundredPosts()" class="btn btn-success"><i class="glyphicon glyphicon-import"></i>&nbsp;Scrape 500 Post</button>

				<button ng-disabled="scraper.fetching" ng-click="scraper.getTodaysPosts()" class="btn btn-primary"><i class="glyphicon glyphicon-import"></i>&nbsp;Scrape todays post</button>

				<button ng-disabled="!scraper.fetching" ng-click="scraper.stop()" class="btn btn-danger"><i class="glyphicon glyphicon-stop"></i>&nbsp;Stop</button>

				<button ng-disabled="!scraper.status.length" ng-click="scraper.status = []" class="btn btn-warning"><i class="glyphicon glyphicon-trash"></i>&nbsp;Clear</button>
			</div>
			<br />
			<h4>Status: </h4>
			<div class="well">
				<div class="alert alert-warning" ng-if="!scraper.status.length">No status to display.</div>
				<pre ng-repeat="status in scraper.status.slice().reverse() track by $index"><b>{{ status }}</b></pre>
			</div>
		</div>
		<!-- Scripts go here -->
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-resource.min.js"></script>
		<script src="http://underscorejs.org/underscore-min.js"></script>
		<script src="/scraper/public/app.js"></script>
	</body>
</html>
