angular.module('phpscrape', ['ngResource'])
    .controller('ScrapeController', function($scope, $http, $q, $sce) {
        var scraper = this;

        var website_url = 'http://phpscrape.dev';

        scraper.status = ['Scrape to start.'];

        scraper.fetching = false;

        scraper.links = 0;

        scraper.link = 'http://buzzfeed.com/index/paging?';

        scraper.params = { p: 100, r: 1 };

        scraper.getFiveHundredPosts = function() {
            
            scraper.fetching = true;

            scraper.status.push('Scraping 500 posts...');

            // scraper.status.push({status: 'danger', message: 'Scraping 500 posts.'});

            scraper.recur();

            // var flattenLinks = _.flatten(scraper.links);

            // scraper.insertToDatabase(flattenLinks);
        }

        scraper.recur = function() {
            console.log('im inside recursion method.');
            if (scraper.links > 500) {
                scraper.status.push('Finished scraping!');
                return;
            }
            scraper.getLinks(website_url + '/scraper/get-links', { link: scraper.link, params: scraper.params })
                .then(function(response) {
                    var links = response.links;
                    scraper.insertToDatabase(links);
                    // scraper.links.push(response.links);
                    scraper.links += links.length;
                    --scraper.params.p;
                    ++scraper.params.r;
                    scraper.recur();
                });
        }

        scraper.getLinks = function(url, params) {
            var deferred = $q.defer();
            $http.post(url, params)
                .success(function(response) {
                    deferred.resolve(response);
                });
            return deferred.promise;
        }

        scraper.insertToDatabase = function(links) {
            angular.forEach(links, function(link) {
                if (link) {
                    $http.post(website_url + '/scraper/extract-insert', {
                        link: link
                    }).success(function() {
                        scraper.status.push(link);
                    });
                }
            });
        }

        scraper.getTodaysPosts = function() {
            scraper.fetching = true;
            scraper.status.push('Scraping todays posts...');
            scraper.link = 'http://buzzfeed.com/';
            $http.post(website_url + '/scraper/today-links', {
                link: scraper.link
            }).success(function(response) {
                scraper.status.push('Extracting data...');
                scraper.insertToDatabase(response.links);
            });
        }

        scraper.stop = function() {
            scraper.status.push('Scraping aborted by user.');
            scraper.fetching = false;
            window.stop();
        }

        $scope.$watch(function() {
            return $http.pendingRequests.length > 0;
        }, function(value) {
            scraper.fetching = value;
        })
    })
