<?php

$app->get('/', function () use ($app) {

    return view()->file('views/master.php');

});

$app->post('/get-links', 'ScrapeController@getLinks');

$app->post('/today-links', 'ScrapeController@getLinksToday');

$app->post('/extract-insert', 'ScrapeController@extractInsert');
