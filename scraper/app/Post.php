<?php

namespace PHPScrape;

use Carbon\Carbon;
use Symfony\Component\DomCrawler\Crawler;

class Post
{

    protected $crawler;

    public $title;

    public $posted_on;

    public $content;

    public $category;

    public $date_created;

    protected $filters = [
        'title' => [
            'filterBy' => '#post-title',
            'getContentBy' => 'text',
        ],
        'content' => [
            'filterBy' => '#buzz_sub_buzz',
            'getContentBy' => 'html',
        ],
        'category' => [
            'filterBy' => 'hgroup > a',
            'getContentBy' => 'text',
        ],
    ];

    public function __construct(Crawler $crawler, $filters = null)
    {
        $this->crawler = $crawler;
        if (!is_null($filters)) {
            $this->filters = $filters;
        }
    }

    public function extract()
    {
        foreach ($this->filters as $key => $value) {
            $crawler = $this->crawler->filter($value['filterBy'])->first();
            $this->{$key} = $crawler->count() > 0 ? trim($crawler->{$value['getContentBy']}()) : '';
        }
        $this->date_created = generateRandomDateBetween('2015-09-09', Carbon::now());
        return $this;
    }

    public function insert()
    {
        if (!get_page_by_title($this->title, 'OBJECT', 'post')) {
            $postID = wp_insert_post([
                'post_title' => $this->title,
                'post_date' => $this->date_created,
                'post_content' => trim($this->content),
            ]);
            wp_set_object_terms($postID, $this->category, 'category');
            return $postID;
        }
        return 0;
    }

}
