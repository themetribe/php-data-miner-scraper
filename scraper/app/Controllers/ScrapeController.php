<?php

namespace PHPScrape\Controllers;

use Goutte\Client;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;
use PHPScrape\Page;
use PHPScrape\Post;

class ScrapeController extends Controller
{

    public $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getLinks(Request $request)
    {
        $page = new Page($this->client, $request->link . http_build_query($request->params));
        return response()->json(['links' => $page->getLinks()]);
    }

    public function getLinksToday(Request $request)
    {
        $page = new Page($this->client, $request->link);
        $links = collect($page->getLinksToday())->reject(function ($link) {
            return empty($link) or is_null($link);
        });
        return response()->json(['links' => $links]);
    }

    public function extractInsert(Request $request)
    {
        $crawler = $this->client->request('GET', $request->link);
        $post = new Post($crawler);
        return $post->extract()->insert();
    }

}
