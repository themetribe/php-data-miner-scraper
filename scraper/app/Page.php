<?php

namespace PHPScrape;

use Carbon\Carbon;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class Page
{
    const URL = 'http://buzzfeed.com';

    protected $crawler;

    protected $links = [];

    protected $url;

    protected $next;

    public function __construct(Client $client, $url)
    {
        // $guzzleClient = new \GuzzleHttp\Client([
        //     'timeout' => 60,
        // ]);
        // $client->setClient($guzzleClient);
        $this->crawler = $client->request('GET', $url);

        $this->url = $url;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getNextUrl()
    {
        return $this->next;
    }

    public function getLinks()
    {
        $this->setLinks();
        return $this->links;
    }

    public function getLinksToday()
    {
        $this->links = $this->crawler->filter('.lede__body')->each(function (Crawler $node) {
            $date_posted = $node->filter('.small-meta__item__time-ago')->first();
            if ($date_posted->count() == 0) {
                return null;
            }
            $date_posted = $date_posted->text();
            $title = $node->filter('.lede__title > a')->first();
            if (is_numeric($date_posted[0])) {
                $date_utc = new Carbon($date_posted);
                if ($date_utc->isToday()) {
                    return self::URL . $title->attr('href');
                }
            }
        });
        return $this->links;
    }

    protected function setLinks()
    {
        $this->links = $this->crawler->filter('.lede__title > a')->each(function ($node) {
            $node_text = $node->text();
            if (!empty($node_text)) {
                return self::URL . $node->attr('href');
            }
        });
    }
}
