<?php

function generateRandomDateBetween($min_date, $max_date)
{
    $randomDate = rand(strtotime($min_date), strtotime($max_date));
    return date('Y-m-d H:i:s', $randomDate);
}
