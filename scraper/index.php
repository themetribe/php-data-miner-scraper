<?php

require_once __DIR__ . '/vendor/autoload.php';

require __DIR__ . '/../wp-blog-header.php';

require __DIR__ . '/app/helpers.php';

ini_set('max_execution_time', 300); // in case if the script timeout

try {
    (new Dotenv\Dotenv(__DIR__ . '/'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

$app = new Laravel\Lumen\Application(
    realpath(__DIR__. '/../')
);

$app->singleton(
    'Illuminate\Contracts\Debug\ExceptionHandler',
    'PHPScrape\Exceptions\Handler'
);
$app->singleton(
    'Illuminate\Contracts\Console\Kernel',
    'PHPScrape\Console\Kernel'
);

$app->group(['namespace' => 'PHPScrape\Controllers'], function ($app) {
    require __DIR__ . '/app/routes.php';
});

$request = Illuminate\Http\Request::capture();

$app->run($request);
