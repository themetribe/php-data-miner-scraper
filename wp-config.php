<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'phpscrape');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pUZnk4P_}M!:uO,p@YIdZ6Y]77>@Hvv @kP?IC<,kUT~:=]YGJ]7YtO8at=+ZahI');
define('SECURE_AUTH_KEY',  ';2yONbgr.56UhHceZv*?iHRrHdJg@xK,^UxaX-wo%;SVW/E)Jci%?Yd5(vnf>tB_');
define('LOGGED_IN_KEY',    '` !U^gq;k&Yl/sHs{gW?zhdou U7_|f,h}QbmP*XP|xU( e/&}xoZPS+S;#V226+');
define('NONCE_KEY',        '_hkr/;u[3X,#5AwfYM,7!hqGWc2rvdL?$pDBGCk/t`I2mahCk)RLi8To]Cgg*:Eb');
define('AUTH_SALT',        '2]k4(>bNSp)M!Uol(R/hFiFq..I$_KZ)o;;r61<nBj;$ygKx|F+/GAItKxm_-%87');
define('SECURE_AUTH_SALT', 'The4g5gj~dIEia;bZMlL:@~L2=PR4$17*|D-4mO*np$b ~.zgWZcG+2jkxB:hdYc');
define('LOGGED_IN_SALT',   'iwz4]7|Q{<!J1yZw{9%X0*@Ur{U/|MfEg~c7v0YcH-5YWQ8Z2}m5^B.+obJO^_%t');
define('NONCE_SALT',       'm`c*lruxikIK^$#fIV%+f,v4_DgI+I)u~4NSwu~bMVfUhDCg9UyvcrS >4<<iwAB');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
