# PHP Scraper#

### Installation setup ###

* run `composer install`

### Installing the database ###

* create table `phpscrape`
* upload the sql file `phpscrape.sql`
* update configs at `wp-config.php`

### Logging in ###

* by default, the username and password is `admin`